// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  // router,
  // template: '<App/>',
  // components: { App },
  data: {
    message: 'Hello World!!'
  }
})

var app2 = new Vue({
  el: '#app-2',
  data: {
    message: 'Yay you did it!'
  }
})

var app3 = new Vue({
  el: '#app-3',
  data: {
    seen:true // False will hide the text
  },
  methods: {
    toggleText: function() {
      if(this.seen == true){
        this.seen = false
      } else {
        this.seen = true
      }
    }
  }
})

var app4 = new Vue({
  el: '#app-4',
  data: {
    todos: [
      {  text: 'Finish this Example' },
      { text: 'Finish this Tutorial' },
      { text: 'Make a Kick-Ass Portfolio site with Vue!' }
    ]
  }
})

var app5 = new Vue({
  el: '#app-5',
  data: {
    message: 'Hello World'
  },
  // Define functions
  methods: {
    reverseMessage: function() {
      this.message = this.message.split('').reverse().join('')
    }
  }
})

var app6 = new Vue({
  el: '#app-6',
  data: {
    message: 'Hello World'
  }
})


// Define A Component
Vue.component('todo-item', {
  // Props are Attributes of a Component
  props: ['todo'],
  template: '<li>{{ todo.text }}</li>'
})

// Using the Component
var app7 = new Vue ({
  el: '#app-7',
  data: {
    shoppingList: [
      {text: 'Duck & Pancakes'},
      {text: 'Chinese Starters'},
      {text: 'Chocolate'}
    ]
  },
  methods: {
    addItem: function(newItem) {
      this.shoppingList.push(newItem)
    }
  }
})
